# KanbanBoard

Project:KanbanBoard <br/>
Made by: <br/>
&nbsp;Camilla Gussoni 856679 <br/>
&nbsp;Giacomo Lenzini 851626 <br/>

Database import instuctrions (PostgreSQL): <br/>

Linux: <br/>
1- Install postgresql on the device; <br/>
2- Create a Database called 'KanbanBoard' using terminal:<br/>
&nbsp;  1. sudo passwd postgres<br/>
&nbsp;  2. su - postgres<br/>
&nbsp;  3. createdb KanbanBoard<br/>
3- Import 'KanbanBoard.sql' to KanbanBoard db:<br/>
&nbsp;  1. pg_restore -U username -d KanbanBoard -1 file_path <br/>

Se si riscontrano errori: <br/>
&nbsp;  -Autenticazione fallita con postgres: sudo passwd postgres<br/>
&nbsp;  -pg_restore impossibile accedere al file passato in input: modificare il file pg_hba.conf (il percorso del file dipende da distro a distro) e modificare i tipi di accesso da peer, ident... a md5<br/>

PgAdmin:<br/>
1-Connect to database server;<br/>
2-Create a database called 'KanbanBoard'<br/>
3-Right click on KanbanBoard datbase -> Restore and select sql script<br/>


File app.go:
Modificare i campi per la connessione al database e la porta dell' host.


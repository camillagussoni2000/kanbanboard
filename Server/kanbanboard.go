package main

import (
	"database/sql"
	"strconv"
	"strings"

	b64 "encoding/base64"

	guuid "github.com/google/uuid"
)

type kanbanBoard struct {
	Name    string `json:"name"`
	Columns []column
}

type column struct {
	ID    string `json:"id"`
	Title string `json:"title"`
	State bool   `json:"state"` //True = In corso,     False = Archviato
	Tiles []tile
}

type tile struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Author      string `json:"author"`
	Content     string `json:"content"`
	Image       []byte `json:"image"`
	ImageBase64 string
	Type        bool `json:"type"` //True = Organizzativo,     False = Informativo
	Text        bool
}

func genUUID() string {
	id := guuid.New()
	return id.String()
}

func genColumnID(columns []column) string {
	idColumn := 0

	if len(columns) == 0 {
		return "0"
	}

	for i := 0; i < len(columns); i++ {
		j, _ := strconv.Atoi(columns[i].ID)
		if j > idColumn {
			idColumn = j
		}
	}
	return strconv.Itoa(idColumn + 1)
}

//Kanbanboard
func (k *kanbanBoard) createKanbanBoard(db *sql.DB) error {
	_, err := db.Exec(`INSERT INTO "Kanbanboard" ("Name") VALUES ($1)`, k.Name)

	if err != nil {
		return err
	}

	columns, _ := getAllColumns(db)

	tiles := []tile{}

	toDo := column{
		ID:    genColumnID(columns),
		Title: "To-Do",
		State: true,
		Tiles: tiles,
	}
	toDo.createColumn(db, *k)

	columns, _ = getAllColumns(db)
	inProgress := column{
		ID:    genColumnID(columns),
		Title: "In Progress",
		State: true,
		Tiles: tiles,
	}
	inProgress.createColumn(db, *k)

	columns, _ = getAllColumns(db)
	done := column{
		ID:    genColumnID(columns),
		Title: "Done",
		State: true,
		Tiles: tiles,
	}
	done.createColumn(db, *k)

	return err
}

func (k *kanbanBoard) getKanbanBoard(db *sql.DB) (kanbanBoard, error) {
	var kQuery kanbanBoard
	err := db.QueryRow(`SELECT "Name" FROM "Kanbanboard" WHERE "Name"=$1`, k.Name).Scan(&kQuery.Name)

	kQuery.Columns, err = kQuery.getColumns(db)
	return kQuery, err
}

func getKanbanBoards(db *sql.DB) ([]kanbanBoard, error) {
	rows, err := db.Query(`SELECT "Name" FROM "Kanbanboard" ORDER BY "Name"`)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	kanbanboards := []kanbanBoard{}

	for rows.Next() {
		var k kanbanBoard
		err := rows.Scan(&k.Name)
		if err != nil {
			return nil, err
		}
		columns, _ := k.getColumns(db)
		k.Columns = columns
		kanbanboards = append(kanbanboards, k)
	}
	return kanbanboards, err
}

func (k *kanbanBoard) deleteKanbanBoard(db *sql.DB) error {
	columns, _ := k.getColumns(db)

	for i := 0; i < len(columns); i++ {
		columns[i].deleteColumn(db)
	}
	_, err := db.Exec(`DELETE FROM "Kanbanboard" WHERE "Name"=$1`, k.Name)
	return err
}

//Column
func getAllColumns(db *sql.DB) ([]column, error) {
	rowsColumns, _ := db.Query(`SELECT "ID", "Title", "State", "Kanbanboard" FROM "Column"`)

	defer rowsColumns.Close()
	var k kanbanBoard

	columns := []column{}

	for rowsColumns.Next() {
		var c column
		err := rowsColumns.Scan(&c.ID, &c.Title, &c.State, &k.Name)
		if err != nil {
			return nil, err
		}

		tiles, err := getTiles(db, c)
		c.Tiles = tiles

		if err != nil {
			return nil, err
		}
		columns = append(columns, c)

	}
	return columns, nil
}
func (k *kanbanBoard) getColumns(db *sql.DB) ([]column, error) {
	rowsColumns, err := db.Query(`SELECT "ID", "Title", "State", "Kanbanboard" FROM "Column" WHERE "Kanbanboard"=$1 ORDER BY "ID"`, k.Name)

	if err != nil {
		return nil, err
	}

	defer rowsColumns.Close()

	columns := []column{}

	for rowsColumns.Next() {
		var c column
		err := rowsColumns.Scan(&c.ID, &c.Title, &c.State, &k.Name)
		if err != nil {
			return nil, err
		}

		tiles, err := getTiles(db, c)
		c.Tiles = tiles

		if err != nil {
			return nil, err
		}
		columns = append(columns, c)

	}
	return columns, nil
}

func (c *column) getColumn(db *sql.DB, k kanbanBoard) (column, error) {
	var cQuery column
	err := db.QueryRow(`SELECT "ID", "Title", "State", "Kanbanboard" FROM "Column" WHERE ("Title"=$2 AND "Kanbanboard"=$1)`, k.Name, c.Title).Scan(&cQuery.ID, &cQuery.Title, &cQuery.State, &cQuery.Tiles)
	return cQuery, err
}
func (c *column) getColumnByID(db *sql.DB) (column, error) {
	var cQuery column
	err := db.QueryRow(`SELECT "ID", "Title", "State", "Kanbanboard" FROM "Column" WHERE "ID"=$1`, c.ID).Scan(&cQuery.ID, &cQuery.Title, &cQuery.State, &cQuery.Tiles)
	return cQuery, err
}

func (c *column) updateColumn(db *sql.DB) error {
	_, err := db.Exec(`UPDATE "Column" SET "ID"="ID", "Title"=$1 WHERE "ID"=$2`, c.Title, c.ID)
	return err
}

func (c *column) storeColumn(db *sql.DB) error {
	_, err := db.Exec(`UPDATE "Column" SET "ID"="ID","State"=false WHERE "ID"=$1`, c.ID)
	return err
}

func (c *column) unstoreColumn(db *sql.DB) error {
	_, err := db.Exec(`UPDATE "Column" SET "ID"="ID","State"=true WHERE "ID"=$1`, c.ID)
	return err
}

func (c *column) deleteColumn(db *sql.DB) error {
	tiles, err := getTiles(db, *c)

	for i := 0; i < len(tiles); i++ {
		tiles[i].deleteTile(db)
	}
	_, err = db.Exec(`DELETE FROM "Column" WHERE "ID"=$1`, c.ID)
	return err
}

func (c *column) createColumn(db *sql.DB, k kanbanBoard) error {
	/* 	k, err := k.getKanbanBoard(db)
	   	if err != nil {
	   		return err
	   	} */

	_, err := db.Exec(`INSERT INTO "Column" ("ID", "Title", "State", "Kanbanboard") VALUES ($1, $2, $3, $4)`, c.ID, c.Title, c.State, k.Name)
	return err
}

//Tile
func getTiles(db *sql.DB, c column) ([]tile, error) {
	rows, err := db.Query(`SELECT "ID", "Title", "Author", "Content", "Image", "Type", "Column" FROM "Tile" WHERE "Column"=$1 ORDER BY "Title"`, c.ID)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	tiles := []tile{}

	for rows.Next() {
		var t tile
		err := rows.Scan(&t.ID, &t.Title, &t.Author, &t.Content, &t.Image, &t.Type, &c.Title)
		t.ImageBase64 = b64.StdEncoding.EncodeToString(t.Image)
		t.Text = true
		if strings.EqualFold(t.Content, "") {
			t.Text = false
		}

		if err != nil {
			return nil, err
		}
		tiles = append(tiles, t)
	}
	return tiles, nil
}

func (t *tile) getTile(db *sql.DB) (tile, column, error) {
	var tQuery tile
	var c column
	err := db.QueryRow(`SELECT "ID", "Title", "Author", "Content", "Image", "Type", "Column" FROM "Tile" WHERE "ID"=$1`, t.ID).Scan(&tQuery.ID, &tQuery.Title, &tQuery.Author, &tQuery.Content, &tQuery.Image, &tQuery.Type, &c.ID)
	tQuery.Text = true
	if strings.EqualFold(tQuery.Content, "") {
		tQuery.Text = false
	}
	return tQuery, c, err
}

func (t *tile) updateTile(db *sql.DB, c column) error {
	_, err := db.Exec(`UPDATE "Tile" SET "Title"=$2, "Author"=$3, "Content"=$4, "Image"=$5, "Type"=$6, "Column"=$7 WHERE "ID"=$1`, t.ID, t.Title, t.Author, t.Content, t.Image, t.Type, c.ID)
	return err
}

func (t *tile) deleteTile(db *sql.DB) error {
	_, err := db.Exec(`DELETE FROM "Tile" WHERE "ID"=$1`, t.ID)
	return err
}

func (t *tile) createTile(db *sql.DB, c column) error {
	_, err := db.Exec(`INSERT INTO "Tile" ("ID", "Title", "Author", "Content", "Image", "Type", "Column") VALUES ($1, $2, $3, $4, $5, $6, $7)`, t.ID, t.Title, t.Author, t.Content, t.Image, t.Type, c.ID)
	return err
}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"strings"
	"text/template"

	"github.com/gorilla/mux"
	"github.com/nfnt/resize"

	_ "github.com/jinzhu/gorm"

	_ "github.com/lib/pq"
)

const (
	Host = "localhost"
	Port = "8080" //Modificare in base alla porta che si desidera usare
)

const (
	host     = "localhost"
	port     = 5432       //Modificare in base alla porta del server del database
	user     = "postgres" //Modificare in base all' utente owner del database
	password = "Lz13!Mk"  //Modificare la pass
	dbname   = "KanbanBoard"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

//Start methods
func (a *App) Initialize() {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	var err error
	a.DB, err = sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}

	jsonString := []byte(`{"Name":"TeamProject"}`)
	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonString))
	req.Header.Set("Content-Type", "application/json")

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	css := http.FileServer(http.Dir("../Front-end/templates/css"))
	js := http.FileServer(http.Dir("../Front-end/templates/js"))
	img := http.FileServer(http.Dir("../Front-end/templates/img"))

	a.Router.HandleFunc("/", LoadHome)

	a.Router.HandleFunc("/Kanbanboards", a.LoadSelectKanbanBoard).Methods("GET")
	a.Router.HandleFunc("/Kanbanboards", a.LoadSelectedKanbanBoard).Methods("POST")

	a.Router.HandleFunc("/Kanbanboard", LoadFormKanbanBoard).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard", a.createKanbanBoard).Methods("POST")
	a.Router.HandleFunc("/Kanbanboard_{ID}/", a.LoadKanbanBoard).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/", a.deleteKanbanBoard).Methods("POST")

	a.Router.HandleFunc("/Kanbanboard_{ID}/Column", LoadCreateColumn).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column", a.createColumn).Methods("POST")

	a.Router.HandleFunc("/Kanbanboard_{ID}/Columns_stored", a.LoadStoredColumns).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Edit", a.LoadEditColumn).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Edit", a.updateColumn).Methods("POST")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Delete", LoadDeleteColumn).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Delete", a.deleteColumn).Methods("POST")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Store", a.storeColumn).Methods("POST")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Unstore", a.unstoreColumn).Methods("POST")

	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Tile", LoadCreateTile).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Column_{IDColumn}/Tile", a.createTile).Methods("POST")

	a.Router.HandleFunc("/Kanbanboard_{ID}/Tile_{IDTile}/Edit", a.LoadEditTile).Methods("GET")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Tile_{IDTile}/Edit", a.updateTile).Methods("POST")
	a.Router.HandleFunc("/Kanbanboard_{ID}/Tile_{IDTile}/Delete", a.deleteTile).Methods("POST")

	a.Router.PathPrefix("/css/").Handler(http.StripPrefix("/css/", css))
	a.Router.PathPrefix("/js/").Handler(http.StripPrefix("/js/", js))
	a.Router.PathPrefix("/img/").Handler(http.StripPrefix("/img/", img))

	http.Handle("/", a.Router)
}

func (a *App) Run() {
	log.Println("Listening on " + Host + ":" + Port + "...")
	log.Fatal(http.ListenAndServe(":8080", a.Router))
}

//Load methods
func LoadHome(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "homepage.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func LoadFormKanbanBoard(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formKanbanboard.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func LoadCreateColumn(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formColumnCreate.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (a *App) LoadEditColumn(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formColumnEdit.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var c column
	vars := mux.Vars(r)
	ID := vars["IDColumn"]
	c.ID = ID

	column, _ := c.getColumnByID(a.DB)

	err = tmpl.Execute(w, column)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func LoadDeleteColumn(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formColumnDelete.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func LoadCreateTile(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formTileCreate.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (a *App) LoadEditTile(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formTileEdit.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	ID := vars["ID"]
	var k kanbanBoard
	k.Name = ID

	columns, _ := k.getColumns(a.DB)

	err = tmpl.Execute(w, columns)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func LoadDeleteTile(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "formTileDelete.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (a *App) LoadStoredColumns(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "storedColumns.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var k kanbanBoard
	vars := mux.Vars(r)
	ID := vars["ID"]
	k.Name = ID

	kanbanboard, _ := k.getKanbanBoard(a.DB)

	err = tmpl.Execute(w, kanbanboard)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (a *App) LoadSelectKanbanBoard(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "selectKanbanboard.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	k, _ := getKanbanBoards(a.DB)

	err = tmpl.Execute(w, k)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (a *App) LoadSelectedKanbanBoard(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		name := r.FormValue("name")
		if strings.EqualFold(name, "") {
			http.Redirect(w, r, "/Kanbanboard", 301)
			return
		}
		var k kanbanBoard
		k.Name = name
		k, err := k.getKanbanBoard(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}

		if len(k.Name) > 0 {
			http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
			return
		}
		http.Redirect(w, r, "/Kanbanboards", 301)
	}
}

func (a *App) LoadKanbanBoard(w http.ResponseWriter, r *http.Request) {
	filePath := path.Join("../Front-end/templates/html", "index.html")
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var k kanbanBoard
	vars := mux.Vars(r)
	ID := vars["ID"]
	k.Name = ID
	k, _ = k.getKanbanBoard(a.DB)
	err = tmpl.Execute(w, k)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//Server methods
func serverError(w http.ResponseWriter, code int, message string) {
	messageJSON(w, code, map[string]string{"error": message})
}

func messageJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

//Database methods
func (a *App) createKanbanBoard(w http.ResponseWriter, r *http.Request) {
	var k kanbanBoard

	if r.Method == "POST" {
		name := r.FormValue("name")
		if strings.EqualFold(name, "") {
			http.Redirect(w, r, "/", 301)
			return
		}

		k.Name = name
		err := k.createKanbanBoard(a.DB)

		if err != nil {
			http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

func (a *App) deleteKanbanBoard(w http.ResponseWriter, r *http.Request) {
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	if r.Method == "POST" {
		err := k.deleteKanbanBoard(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/", 301)
	}
}

func (a *App) getKanbanBoard(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ID := vars["ID"]
	var k kanbanBoard
	k.Name = ID

	k, err := k.getKanbanBoard(a.DB)
	if err != nil {
		serverError(w, http.StatusInternalServerError, err.Error())
		return
	}
}

//Columns
func (a *App) createColumn(w http.ResponseWriter, r *http.Request) {
	var c column
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	if r.Method == "POST" {
		title := r.FormValue("title")
		var cTest column
		cTest.Title = title
		column, err := cTest.getColumn(a.DB, k)

		if strings.EqualFold(title, "") || len(column.Title) > 0 {
			http.Redirect(w, r, "./", 301)
			return
		}

		state := r.Form["radio"]
		if strings.EqualFold(state[0], "") {
			serverError(w, http.StatusBadRequest, "Invalid state")
			return
		}

		columns, _ := getAllColumns(a.DB)
		c.ID = genColumnID(columns)
		c.Title = title
		switch state[0] {
		case "In-Progress":
			c.State = true
		case "Stored":
			c.State = false
		}

		err = c.createColumn(a.DB, k)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

func (a *App) updateColumn(w http.ResponseWriter, r *http.Request) {
	var c column
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	idColumn := vars["IDColumn"]
	c.ID = idColumn

	if r.Method == "POST" {
		title := r.FormValue("title")
		var cTest column
		cTest.Title = title
		column, err := cTest.getColumn(a.DB, k)

		if strings.EqualFold(title, "") || len(column.Title) > 0 {
			http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
			return
		}

		c.Title = title

		err = c.updateColumn(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

func (a *App) storeColumn(w http.ResponseWriter, r *http.Request) {
	var c column
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	idColumn := vars["IDColumn"]
	c.ID = idColumn

	if r.Method == "POST" {
		err := c.storeColumn(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

func (a *App) unstoreColumn(w http.ResponseWriter, r *http.Request) {
	var c column
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	idColumn := vars["IDColumn"]
	c.ID = idColumn

	if r.Method == "POST" {
		err := c.unstoreColumn(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/Columns_stored", 301)
	}
}

func (a *App) deleteColumn(w http.ResponseWriter, r *http.Request) {
	var c column
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	idColumn := vars["IDColumn"]
	c.ID = idColumn

	if r.Method == "POST" {
		err := c.deleteColumn(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

func (a *App) getTiles(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["idColumn"]
	var c column
	c.ID = id

	tiles, err := getTiles(a.DB, c)
	if err != nil {
		serverError(w, http.StatusInternalServerError, err.Error())
		return
	}
	messageJSON(w, http.StatusOK, tiles)
}
func (a *App) updateTile(w http.ResponseWriter, r *http.Request) {
	var t tile
	var k kanbanBoard

	var actualTile tile

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	t.ID = vars["IDTile"]

	actualTile, c, _ := t.getTile(a.DB)

	if r.Method == "POST" {
		name := r.FormValue("name")
		c.Title = name
		if !strings.EqualFold(name, "") {
			column, _ := c.getColumn(a.DB, k)
			c.ID = column.ID
		}

		title := r.FormValue("title")
		if strings.EqualFold(title, "") {
			title = actualTile.Title
		}

		author := r.FormValue("author")
		if strings.EqualFold(author, "") {
			author = actualTile.Author
		}

		checkContent := r.Form["type"]

		switch checkContent[0] {
		case "Text":
			content := r.FormValue("content")
			t.Content = content
			if strings.EqualFold(content, "") {
				t.Content = actualTile.Content
			}
		case "Image":
			check := false

			file, _, err := r.FormFile("image")
			if err != nil {
				if actualTile.Image != nil {
					t.Image = actualTile.Image
					check = true
				}
			}
			if err == nil {
				defer file.Close()
			}

			if check == false {
				file, _, _ := r.FormFile("image")
				imgSize, _, _ := image.DecodeConfig(file)
				defer file.Close()

				if imgSize.Width > 900 || imgSize.Height > 900 {
					file, _, _ := r.FormFile("image")
					img, _, _ := image.Decode(file)
					defer file.Close()
					imageResized := resize.Resize(900, 900, img, resize.Lanczos3)
					buf := new(bytes.Buffer)
					err = jpeg.Encode(buf, imageResized, nil)

					imgFile := buf.Bytes()
					t.Image = imgFile
				}
				if !(imgSize.Width > 900 || imgSize.Height > 900) {
					fileImg, _, _ := r.FormFile("image")
					image, _ := ioutil.ReadAll(fileImg)
					t.Image = image
				}
			}
		}

		t.Title = title
		t.Author = author
		t.Type = actualTile.Type

		err := t.updateTile(a.DB, c)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}
func (a *App) deleteTile(w http.ResponseWriter, r *http.Request) {
	var t tile
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	idTile := vars["IDTile"]
	t.ID = idTile

	if r.Method == "POST" {
		err := t.deleteTile(a.DB)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

func (a *App) createTile(w http.ResponseWriter, r *http.Request) {
	var t tile
	var c column
	var k kanbanBoard

	vars := mux.Vars(r)
	id := vars["ID"]
	k.Name = id

	c.ID = vars["IDColumn"]

	if r.Method == "POST" {
		title := r.FormValue("title")

		author := r.FormValue("author")

		checkContent := r.Form["type"]

		switch checkContent[0] {
		case "Text":
			content := r.FormValue("content")
			t.Content = content
		case "Image":
			check := false

			file, _, err := r.FormFile("image")
			if err != nil {
				t.Image = nil
				check = true
			}
			if err == nil {
				defer file.Close()
			}

			if check == false {
				file, _, _ := r.FormFile("image")
				imgSize, _, _ := image.DecodeConfig(file)
				defer file.Close()

				if imgSize.Width > 900 || imgSize.Height > 900 {
					file, _, _ := r.FormFile("image")
					img, _, _ := image.Decode(file)
					defer file.Close()
					imageResized := resize.Resize(900, 900, img, resize.Lanczos3)
					buf := new(bytes.Buffer)
					err = jpeg.Encode(buf, imageResized, nil)

					imgFile := buf.Bytes()
					t.Image = imgFile
				}
				if !(imgSize.Width > 900 || imgSize.Height > 900) {
					fileImg, _, _ := r.FormFile("image")
					image, _ := ioutil.ReadAll(fileImg)
					t.Image = image
				}
			}
		}

		if strings.EqualFold(title, "") {
			http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
			return
		}

		state := r.Form["radio"]
		if strings.EqualFold(state[0], "") {
			serverError(w, http.StatusBadRequest, "Invalid state")
			return
		}
		t.ID = genUUID()
		t.Title = title
		t.Author = author
		switch state[0] {
		case "Organizative":
			t.Type = true
		case "Informative":
			t.Type = false
		}

		err := t.createTile(a.DB, c)

		if err != nil {
			serverError(w, http.StatusInternalServerError, err.Error())
			return
		}
		http.Redirect(w, r, "/Kanbanboard_"+k.Name+"/", 301)
	}
}

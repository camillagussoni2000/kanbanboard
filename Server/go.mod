module github.com/GiacoLenzo2109/KanbanBoard

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.10.2
	github.com/libvips/libvips v8.11.1+incompatible // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
)

PGDMP         8                y           KanbanBoard    13.3    13.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16681    KanbanBoard    DATABASE     b   CREATE DATABASE "KanbanBoard" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE "KanbanBoard";
                postgres    false            �            1259    16682    Column    TABLE     �   CREATE TABLE public."Column" (
    "Title" character varying NOT NULL,
    "State" boolean NOT NULL,
    "Kanbanboard" character varying NOT NULL,
    "ID" character varying NOT NULL
);
    DROP TABLE public."Column";
       public         heap    postgres    false            �            1259    16688    Kanbanboard    TABLE     M   CREATE TABLE public."Kanbanboard" (
    "Name" character varying NOT NULL
);
 !   DROP TABLE public."Kanbanboard";
       public         heap    postgres    false            �            1259    16694    Tile    TABLE       CREATE TABLE public."Tile" (
    "ID" character varying NOT NULL,
    "Title" character varying NOT NULL,
    "Author" character varying NOT NULL,
    "Content" character varying,
    "Type" boolean NOT NULL,
    "Column" character varying NOT NULL,
    "Image" bytea
);
    DROP TABLE public."Tile";
       public         heap    postgres    false            �          0    16682    Column 
   TABLE DATA                 public          postgres    false    200   E       �          0    16688    Kanbanboard 
   TABLE DATA                 public          postgres    false    201   _       �          0    16694    Tile 
   TABLE DATA                 public          postgres    false    202   y                  2606    16701    Column Column_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public."Column"
    ADD CONSTRAINT "Column_pkey" PRIMARY KEY ("ID");
 @   ALTER TABLE ONLY public."Column" DROP CONSTRAINT "Column_pkey";
       public            postgres    false    200                       2606    16703    Kanbanboard Kanbanboard_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."Kanbanboard"
    ADD CONSTRAINT "Kanbanboard_pkey" PRIMARY KEY ("Name");
 J   ALTER TABLE ONLY public."Kanbanboard" DROP CONSTRAINT "Kanbanboard_pkey";
       public            postgres    false    201                       2606    16705    Tile Tile_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Tile"
    ADD CONSTRAINT "Tile_pkey" PRIMARY KEY ("ID");
 <   ALTER TABLE ONLY public."Tile" DROP CONSTRAINT "Tile_pkey";
       public            postgres    false    202                        2606    16706    Tile Column    FK CONSTRAINT     ~   ALTER TABLE ONLY public."Tile"
    ADD CONSTRAINT "Column" FOREIGN KEY ("Column") REFERENCES public."Column"("ID") NOT VALID;
 9   ALTER TABLE ONLY public."Tile" DROP CONSTRAINT "Column";
       public          postgres    false    3866    200    202                       2606    16711    Column kanbanboard_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public."Column"
    ADD CONSTRAINT kanbanboard_fk FOREIGN KEY ("Kanbanboard") REFERENCES public."Kanbanboard"("Name");
 A   ALTER TABLE ONLY public."Column" DROP CONSTRAINT kanbanboard_fk;
       public          postgres    false    200    3868    201            �   
   x���          �   
   x���          �   
   x���         